
const dataBase = {
  host: 'localhost', // 服务器地址
  username: 'root', // mysql用户名称
  password: "admin123", // mysql用户密码
  port: '3306', // 端口
  database: 'nodesql', // 数据库名称
  dialect: 'mysql'
}
export default dataBase
