var sql:any = {};
const {HOSTNAME,DATABASE,USER,PASSWORD,PREFIX}=process.env
sql.mysql={
    host:HOSTNAME,  //mysql的安装主机地址
    user:USER,        //访问mysql的用户名
    password:PASSWORD, // 访问mysql的密码
    database:DATABASE,    //访问mysql的数据库名
    prefix:PREFIX
}

export default  sql
