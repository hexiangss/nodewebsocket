import SocketsXinghuo from "./chatxinghuo";
const xinghuo = new SocketsXinghuo();
export const sendAiMessage = (str: string): Promise<string> => {
  return new Promise(async (resolve, reject) => {
    console.log(xinghuo)
    xinghuo
      .send(str)
      .then((text) => {
        resolve(text);
      })
      .catch((err) => {
        console.log(err)
        reject()
      })
      .finally(() => {});
  });
};
