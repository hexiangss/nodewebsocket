export interface UserInfoFace{
    userId:string
    userName:string
    ws?:any
    isNew?:boolean,
    sex?:number
    ws?:any
}
export interface LoginFace{
    userName:string
    passWord:string
}

export interface UserFace{
    id?:string
    username:string
    nickname?:string
    password?:string,
    sex?:number
    sort?:number
    webid?:string
}