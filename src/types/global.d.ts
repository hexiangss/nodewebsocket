import nodeLittleDb from 'node_modules/node-little-db/dist/types/index'
declare  global {

declare module "node-little-db" {
  return nodeLittleDb
}
  declare interface IResponse<T = any> {
    code: string;
    data: T extends any ? T : T & any;
  }
  declare type nodeLittleDb = any;
  declare interface Fn<T = any> {
    (...arg: T[]): T;
  }
  declare type Nullable<T> = T | null;
  declare type ElRef<T extends HTMLElement = HTMLDivElement> = Nullable<T>;
  declare type Recordable<T = any, K = string> = Record<
    K extends null | undefined ? string : K,
    T
  >;
  declare type ComponentRef<T> = InstanceType<T>;

  declare type IDType = string | number;
  declare type LocaleType = "zh-CN" | "en";
  declare type AxiosHeaders =
    | "application/json"
    | "application/x-www-form-urlencoded"
    | "multipart/form-data";
  declare type AxiosMethod = "get" | "post" | "delete" | "put";
  declare type AxiosResponseType =
    | "arraybuffer"
    | "blob"
    | "document"
    | "json"
    | "text"
    | "stream";
  declare interface AxiosConfig {
    params?: any;
    data?: any;
    url?: string;
    method?: AxiosMethod;
    headersType?: string;
    responseType?: AxiosResponseType;
  }
  declare interface ListQueryParams {
    size: number;
    current: number;
  }

  // 列表的返回结果，
  declare interface IResponseList<T = any> {
    data: {
      list: T extends any ? T : T & any;
      total: number;
      records: T extends any ? T : T & any;
    };
    code: string;
  }
  // 把指定属性必填，其他可选 declare type ChatFacePing = SetOptional<ChatFace, 'type'|'userId'>;
  declare type SetOptional<T, K extends keyof T> = Simplify<
    Partial<Pick<T, K>> & Pick<T, Exclude<keyof T, K>>
  >;

  declare type RGB = `rgb(${number}, ${number}, ${number})`;
  declare type RGBA = `rgba(${number}, ${number}, ${number}, ${number})`;
  declare type HEX = `#${string}`;

  declare type Color = RGB | RGBA | HEX;
}


export default global;
