
type SendType='chat'|'system' |string

export interface SocketDataFace{
    type:SendType
    msg?:string
    count?:number
    qipu?:any
    qp?:any
    name?:string
    status?:string,
    player?:any
    createTime?:number
}