/**
 * 特殊的错误返回信息
 */
export const CodeEnum = {
  CODE_LIMITED: {
    code: 240002,
    msg: "60秒不能重复发送",
  },
  CODE_SEND: {
    code: 240003,
    msg: "请发送图形验证码",
  },
  CODE_ERROR: {
    code: 240004,
    msg: "图形验证码错误",
  },
  ACCOUNT_REPEAT: {
    code: 250001,
    msg: "账号已经存在",
  },
  ACCOUNT_UNREGISTER: {
    code: 250002,
    msg: "账号不存在",
  },
  ACCOUNT_PWD_ERROR: {
    code: 250003,
    msg: "账号或者密码错误",
  },
  ACCOUNT_UNLOGIN: {
    code: 250004,
    msg: "账号未登录",
  },
  ACCOUNT_REPEAT_LOGIN: {
    code: 250006,
    msg: "账号在其它设备登录，踢下线",
  },
  ACCOUNT_DISABLED: {
    code: 250007,
    msg: "该账号已冻结，请联系管理员",
  },
  ACCOUNT_STATUS_EXCEPTION: {
    code: 250008,
    msg: "账号状态异常，请联系管理员",
  },

  FILE_UPLOAD_USER_IMG_FAIL: {
    code: 700101,
    msg: "用户头像上传失败",
  },
};


interface BackCodeFace{
    code:number
    data:any
    msg:string
}

/**
 * 返回值统一管理工具
 */
class BackCode {
    code:number
    data:any
    msg:string
  constructor({ code, data, msg }:BackCodeFace) {
    this.code = code;
    this.data = data;
    this.msg = msg;
  }
  // 请求成功，只返回code内容
  static buildSuccess() {
    return new BackCode({ code: 0, data: null, msg: null }).toJson();
  }
  // 请求成功，只返回code+data内容
  static buildSuccessAndData(data:any) {
    return new BackCode({ code: 200, data, msg: null }).toJson();
  }
  // 请求失败，只返回code+msg内容
  static buildError(msg:string) {
    return new BackCode({ code: -1, data: null, msg }).toJson();
  }
  // 自定义返回code+msg内容
  static buildResult(codeEnum:any) {
    return new BackCode({
      code: codeEnum.code,
      data: null,
      msg: codeEnum.msg,
    }).toJson();
  }
  toJson() {
    return {
      code: this.code,
      data: this.data,
      msg: this.msg,
    };
  }
}

export default  BackCode
