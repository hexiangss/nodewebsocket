import type { server } from "websocket";
import type { SocketDataFace } from "@/types/socket";
import { addFile, getFile, getFileLength } from "./file";

type TypeFace = "chat" | "system" | "wuziqi" | "qita" | "online" | "ai";

const getName = (type: TypeFace) => {
  const date = new Date();
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const rootPath = __dirname; //process.cwd();
  const name = `${year}${month}${day}`;
  const url = rootPath + `../../log/msg/${year}/${month}/${type}${day}.log`;
  return url;
};

/**
 * 发送消息类
 */
class sendMsgClass {
  wsServer: server;
  msgList: any;
  sqlUrl?: string;
  constructor(wsServer: server) {
    this.wsServer = wsServer;
    this.msgList = [];
    this.getMsgList();
  }
  getMsgList() {
    const url = getName("chat");
    const arr = getFileLength(url);

    return arr;
  }
  getPrevMsg() {
    let data;
    this.msgList.reverse().some((v: any, index: number) => {
      if (v.userId && index > 0) {
        data = v;
        return true;
      }
      return false;
    });
    return data;
  }
  addMsg(data: any) {
    if (!data || !data.type) return;
    const { type } = data;
    this.msgList.push(data);
    if (this.msgList.length > 80) {
      this.msgList = this.msgList.slice(-80);
    }
    let url: any;
    switch (type) {
      case "chat":
      case "wuziqi":
      case "system":
      case "online":
        url = getName(type);
        break;
      case "ai":
        url = getName("chat");
        break;
      default:
        url = getName("qita");
        break;
    }
    if (!url) return;
    addFile(url, data, "a");
  }

  // 广播通知
  broadcast(info: SocketDataFace, isAddhistory = true) {
    info.count = this.wsServer.connections.length;
    info.createTime = new Date().getTime();
    this.wsServer.connections.forEach(function (conn) {
      conn.send(JSON.stringify(info));
    });
    if (isAddhistory) {
      this.addMsg(info);
    }
    info = undefined;
  }
  /**
   * 发送聊天消息
   * @param obj
   */
  sendChat = (obj: any) => {
    this.broadcast({
      ...obj,
      status: 'txt'
    });
  };
  /**
 * 发送聊天消息
 * @param obj
 */
  sendChatImg = (obj: any) => {
    this.broadcast({
      ...obj,
      status: 'image'
    });
  };
  /**
   * 发送系统消息
   * @param msg
   */
  sendSystem = (msg: string, ws?: any) => {
    const obj = {
      type: "system",
      msg,
    };
    if (ws) {
      ws.send(JSON.stringify(obj));
    } else {
      this.broadcast(obj);
    }
  };

  /**
   * 发送五子棋系统消息
   * @param msg
   */
  sendWzqSystem = (msg: string, ws?: any) => {
    const obj = {
      type: "wuziqi",
      status: "system",
      msg,
    };
    if (ws) {
      ws.send(JSON.stringify(obj));
    } else {
      this.broadcast(obj);
    }
  };

  /**
   * 发送五子棋棋盘
   * @param ws 如果携带了 则单个发送
   */
  sendWzq = (qp: any, ws?: any) => {
    const obj = {
      type: "wuziqi",
      status: "qp",
      qp,
    };
    if (ws) {
      ws.send(JSON.stringify(obj));
    } else {
      this.broadcast(obj, false);
    }
  };
  /**
   * 发送五子棋棋手
   * @param qsList
   */
  sendWzqQs = (qsList: any, ws?: any) => {
    const obj = {
      type: "wuziqi",
      status: "qs",
      qsList: qsList,
    };
    if (ws) {
      ws.send(JSON.stringify(obj));
    } else {
      this.broadcast(obj, false);
    }
  };
  /**
   * 发送历史消息
   * @param ws
   */
  sendHistoryMsg = (ws: any) => {
    const list = this.getMsgList();
    const obj = {
      type: "msgList",
      msgList: list,
    };
    ws.send(JSON.stringify(obj));
  };
}

export interface SendMsgType extends sendMsgClass { }

export default sendMsgClass;
