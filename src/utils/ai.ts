const editNameArr = [
  "我要改名字",
  "我要改名",
  "改名字",
  "修改昵称",
  "修改名字",
  "我是"
];
const sexArr = ["男人", "女人", "男", "女", "男的", "女的"];
const getsexArr = ["是男是女", "你是男的还是女的", "你是男还是女", "你是男的女的", "你是男人还是女人"];
const aiNameArr = [...editNameArr, ...sexArr, ...getsexArr];

const isAi = (msg: string) => {
  return aiNameArr.some((v) => msg.indexOf(v) === 0);
};

const aiUpdateName = (msg: string) => {
  let name;
  editNameArr.some((v) => {
    if (msg.indexOf(v) === 0) {
      name = msg.slice(v.length).replace(/[:：]/g, "");
      return true;
    } else {
      return false;
    }
  });
  return name;
};

const aiUpdateSex = (msg: string) => {
  return sexArr.includes(msg);
};
/**
 * 根据聊天上一条记录获取是男是女
 * @param msg 
 * @returns 
 */
const aiGetSex = (msg: string) => {
  return getsexArr.includes(msg);
};

/**
 * 想知道别人是男是女
 * @param msg 
 * @returns 
 */

const aiUserGetSex = (msg: string) => {
  let name;
  getsexArr.some((v) => {
    const index = msg.indexOf(v)
    if (index > -1) {
      name = msg.slice(0, index).replace(/[:：]/g, "");
      return true;
    } else {
      return false;
    }
  });
  return name;
};







/**
 *
 * @param msg 根据消息判断是不是Ai
 * @returns
 */
export const Ai = (msg: string) => {


  if (isAi(msg)) {
    const name = aiUpdateName(msg);
    if (name) {
      return {
        cmd: "editname",
        val: name
      };
    } else if (aiUpdateSex(msg)) {
      const sex = msg.includes("男") ? 1 : 2;
      return {
        cmd: "editSex",
        val: sex,
      };
    } else if (aiGetSex(msg)) {
      return {
        cmd: "cmd:getPrevSex",
        val: '',
      };
    } else if (aiUserGetSex(msg)) {
      return {
        cmd: "cmd:getUserSex",
        val: '',
      };
    }

  } else {
    return false;
  }
};
