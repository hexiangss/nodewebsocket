import Express from "express";
import BackCode from "../../utils/backCode";
import { info, add, login } from "../../severs/adminUser";
import { varifyToken } from "../../plugins/jwt";
const router: any = Express.Router();
router.get("/info", function (req: any, res: any, next: any) {
  const token = req.headers["authorization"]?.split(" ")[1];
  const payload = varifyToken(token);
  const { id } = payload;
  info(id).then((user) => {
    if (user) {
      const data = BackCode.buildSuccessAndData(user);
      res.send(data);
    } else {
      let obj = BackCode.buildError("用户信息获取失败");
      res.send(obj);
    }
  });

  // res.render("index", { title: "Express" });
});
export default router;
