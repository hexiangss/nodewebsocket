import Express from "express";
import login from "./back/login";
import user from "./back/user";
let app:any = Express();
app.use("/user",user);
app.use("/login",login);
export default app
