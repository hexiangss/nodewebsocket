import { sqlQuery } from "../../db/resetSet/index";
import type { UserInfoFace } from "@/types/user";

export const list = () => {
  return new Promise((resolve, reject) => {
    const sql = "SELECT * FROM `xh_admin_user`";
    sqlQuery(sql).then((res) => {
      resolve(res);
    });
  });
};
